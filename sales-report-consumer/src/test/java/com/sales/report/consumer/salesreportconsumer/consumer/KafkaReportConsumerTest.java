package com.sales.report.consumer.salesreportconsumer.consumer;

import com.sales.report.models.SalesReportData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class KafkaReportConsumerTest {

    @Mock
    private SimpMessagingTemplate messagingTemplate;

    private KafkaReportConsumer kafkaReportConsumer;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        kafkaReportConsumer = new KafkaReportConsumer(messagingTemplate);
    }

    @Test
    public void testListenToSalesReports() {
        // Arrange
        SalesReportData salesReportData = new SalesReportData();

        // Act
        kafkaReportConsumer.listenTomProducts(salesReportData);

        // Assert
        verify(messagingTemplate, times(1)).convertAndSend(null, salesReportData);
    }
}
