package com.sales.report.consumer.salesreportconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalesReportConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalesReportConsumerApplication.class, args);
	}

}
