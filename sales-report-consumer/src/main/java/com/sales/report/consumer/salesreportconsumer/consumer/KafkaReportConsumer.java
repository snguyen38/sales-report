package com.sales.report.consumer.salesreportconsumer.consumer;

import com.sales.report.models.SalesReportData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaReportConsumer {
    private static final Logger LOGGER = LogManager.getLogger(KafkaReportConsumer.class);
    private final SimpMessagingTemplate messagingTemplate;

    @Value(value = "${websocket.topic}")
    private String websocketTopic;

    @Autowired
    public KafkaReportConsumer(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @KafkaListener(topics = "${kafka.topic}", containerFactory = "kafkaListenerContainerFactory")
    public void listenTomProducts(@Payload SalesReportData message) {
        LOGGER.info("consume message from kafka");
        messagingTemplate.convertAndSend(websocketTopic, message);
    }
}
