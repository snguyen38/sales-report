# Sales Report Consumer

This project subscribe a Kafka's topic then send data to UI via websocket.

## Installation
1. Clone the repository
2. Install the necessary dependencies by running gradle command

    `.\gradlew clean build`
3. Navigate to the project's docker root directory

   `cd docker`
4. Run the service by running docker compose command in the root docker folder.
   Please note that Kafka has to be running already.

   `docker-compose up`


## Configuration
The configuration file `application.properties` contains the following options:
* spring.kafka.server.url: The address of the Kafka broker to subscribe to.
* kafka.topic: The Kafka's topic
* websocket.topic: The STOMP's topic websocket