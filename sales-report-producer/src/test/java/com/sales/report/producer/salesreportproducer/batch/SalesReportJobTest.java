package com.sales.report.producer.salesreportproducer.batch;

import com.sales.report.models.SalesReportData;
import com.sales.report.producer.salesreportproducer.producers.KafkaProducer;
import com.sales.report.producer.salesreportproducer.utils.FileReaderUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

class SalesReportJobTest {

    @Mock
    private KafkaProducer kafkaProducer;

    @Mock
    private FileReaderUtils fileReaderUtils;

    @InjectMocks
    private SalesReportJob salesReportJob;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        salesReportJob = new SalesReportJob(kafkaProducer, fileReaderUtils);
    }

    @Test
    void processFileScheduler_shouldReadFilesAndSendDataToKafka() {
        // Given
        SalesReportData salesReportData = new SalesReportData();
        when(fileReaderUtils.readFilesAndCalculateSalesData()).thenReturn(salesReportData);

        // When
        salesReportJob.processFileScheduler();

        // Then
        verify(fileReaderUtils, times(1)).readFilesAndCalculateSalesData();
        verify(kafkaProducer, times(1)).sendMessage(salesReportData);
    }
}

