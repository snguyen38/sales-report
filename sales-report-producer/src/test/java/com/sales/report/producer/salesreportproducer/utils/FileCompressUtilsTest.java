package com.sales.report.producer.salesreportproducer.utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FileCompressUtilsTest {

    private FileCompressUtils fileCompressUtils;
    private String testDirectory = "test-dir";

    @BeforeEach
    void setUp() {
        fileCompressUtils = new FileCompressUtils();
    }

    @Test
    void testCompressFile() throws IOException {
        // Set up input and output file paths
        String inputFilePath = "/testfile.txt";
        String outputDirectoryPath = System.getProperty("java.io.tmpdir");
        String outputFilePath = outputDirectoryPath + File.separator + testDirectory;
        File directory = new File(outputFilePath);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        // Compress the file
        fileCompressUtils.compressFile(inputFilePath, outputFilePath);

        // Verify that the compressed file was created with the expected name
        String expectedFileNamePattern = "testfile.txt_\\d{8}_\\d{6}.zip";
        File[] outputFiles = new File(outputFilePath).listFiles();
        assertTrue(outputFiles.length > 0);
        assertTrue(outputFiles[0].getName().matches(expectedFileNamePattern));
        directory.delete();
    }

    @AfterEach
    public void tearDown() {
        String outputDirectoryPath = System.getProperty("java.io.tmpdir");
        String outputFilePath = outputDirectoryPath + File.separator + testDirectory;
        // Delete the test directory and its contents
        File directory = new File(outputFilePath);
        for (File file : directory.listFiles()) {
            file.delete();
        }
        directory.delete();
    }
}

