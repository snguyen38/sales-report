package com.sales.report.producer.salesreportproducer.producers;

import com.sales.report.models.SalesReportData;
import com.sales.report.models.SalesReportItem;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;

import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class KafkaProducerTest {

    @Mock
    private KafkaTemplate<String, SalesReportData> kafkaTemplate;

    @Captor
    private ArgumentCaptor<Message<SalesReportData>> messageCaptor;

    private KafkaProducer kafkaProducer;


    @BeforeEach
    public void setup() {
        kafkaProducer = new KafkaProducer(kafkaTemplate);
    }

    @Test
    public void testSendMessage() {
        SalesReportData salesReportData = new SalesReportData();
        salesReportData.getReportMessages().add(new SalesReportItem("test product", "test store", 1, 1));
        when(kafkaTemplate.send(any(Message.class))).thenReturn(mockSendResult());

        kafkaProducer.sendMessage(salesReportData);

        verify(kafkaTemplate, times(1)).send(messageCaptor.capture());
        Message<SalesReportData> capturedMessage = messageCaptor.getValue();

        // verify that the payload were set on the captured message
        assertEquals(salesReportData, capturedMessage.getPayload());
    }

    private CompletableFuture<SendResult<String, SalesReportData>> mockSendResult() {
        return new CompletableFuture<>() {
            @Override
            public SendResult<String, SalesReportData> get() {
                return new SendResult<>(new ProducerRecord<>("test-topic", new SalesReportData()), null);
            }
        };
    }

}

