package com.sales.report.producer.salesreportproducer.utils;

import com.sales.report.models.SalesReportData;
import com.sales.report.models.SalesReportItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;

public class FileReaderUtilsTest {

    private FileReaderUtils fileReaderUtils;

    @Mock
    private FileCompressUtils fileCompressUtils;

    @BeforeEach
    public void setUp() throws IOException {
        MockitoAnnotations.openMocks(this);
        fileReaderUtils = new FileReaderUtils(fileCompressUtils);
        Mockito.doNothing().when(fileCompressUtils).compressFile(anyString(), anyString());
    }

    @Test
    public void testReadFilesAndCalculateSalesData() {
        SalesReportData salesReportData = fileReaderUtils.readFilesAndCalculateSalesData();
        assertNotNull(salesReportData);
        List<SalesReportItem> reportMessages = salesReportData.getReportMessages();
        assertNotNull(reportMessages);
        assertFalse(reportMessages.isEmpty());

        // Check the first sales report item
        SalesReportItem firstItem = reportMessages.get(0);
        assertNotNull(firstItem);
        assertEquals("Apple iPad 9", firstItem.getProduct());
        assertEquals("Store 7", firstItem.getStore());
        assertEquals(57, firstItem.getTotalSalesUnit());
        assertEquals(31069.9389, firstItem.getTotalSalesRevenue());

        // Check the second sales report item
        SalesReportItem secondItem = reportMessages.get(1);
        assertNotNull(secondItem);
        assertEquals("Apple iPad 9", secondItem.getProduct());
        assertEquals("Store 1", secondItem.getStore());
        assertEquals(94, secondItem.getTotalSalesUnit());
        assertEquals(52164.6657, secondItem.getTotalSalesRevenue());
    }
}
