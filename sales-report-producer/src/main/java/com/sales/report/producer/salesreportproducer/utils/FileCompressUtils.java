package com.sales.report.producer.salesreportproducer.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class FileCompressUtils {

    private static final Logger LOGGER = LogManager.getLogger(FileCompressUtils.class);

    public void compressFile(String inputFilePath, String outputFilePath)  throws IOException {
        // Create a file input stream for the input file
        InputStream inputStream = FileReaderUtils.class.getResourceAsStream(inputFilePath);

        // Create a file output stream for the output file
        File inputFile = new File(inputFilePath);
        File outputFile = new File(outputFilePath + File.separator + generateFileName(inputFile.getName()));
        String outputCompressFilePath = outputFile.getAbsolutePath();
        LOGGER.info("outputCompressFilePath === {}", outputCompressFilePath);
        FileOutputStream outputStream = new FileOutputStream(outputCompressFilePath);

        // Create a zip output stream to compress the file
        ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);

        // Create a zip entry for the file
        ZipEntry zipEntry = new ZipEntry(inputFile.getName());

        // Add the zip entry to the zip output stream
        zipOutputStream.putNextEntry(zipEntry);

        // Create a buffer to read the file in chunks
        byte[] buffer = new byte[1024];
        int length;

        // Read the file and write it to the zip output stream
        while ((length = inputStream.read(buffer)) > 0) {
            zipOutputStream.write(buffer, 0, length);
        }

        // Close the streams
        zipOutputStream.closeEntry();
        zipOutputStream.close();
        inputStream.close();
        outputStream.close();
        LOGGER.info("zip file complete with path === {}", outputCompressFilePath);
    }

    private String generateFileName(String originalFileName) {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String dateString = dateFormat.format(date);
        return originalFileName + "_" + dateString + ".zip";
    }
}
