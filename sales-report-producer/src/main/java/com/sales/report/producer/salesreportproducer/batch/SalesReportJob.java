package com.sales.report.producer.salesreportproducer.batch;


import com.sales.report.models.SalesReportData;
import com.sales.report.producer.salesreportproducer.producers.KafkaProducer;
import com.sales.report.producer.salesreportproducer.utils.FileReaderUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class SalesReportJob {
    private static final Logger LOGGER = LogManager.getLogger(SalesReportJob.class);

    private final KafkaProducer kafkaProducer;
    private final FileReaderUtils fileReaderUtils;

    @Autowired
    public SalesReportJob(KafkaProducer kafkaProducer, FileReaderUtils fileReaderUtils) {
        this.kafkaProducer = kafkaProducer;
        this.fileReaderUtils = fileReaderUtils;
    }

    /**
     * This scheduler job runs every minute and performs the following tasks:
     * 1) Reads the current file containing data from October, September, and December, and aggregates the data.
     * 2) Sends the aggregated data to Kafka.
     * 3) Archives the file to a zip folder - app/archives
     *
     * Note: This job assumes that the file containing data from October, September, and December
     * is located in a specific directory that is accessible to the application.
     */
    @Scheduled(fixedDelay = 60000) // 1 min - time in millisecond
    public void processFileScheduler() {
        SalesReportData salesReportData = fileReaderUtils.readFilesAndCalculateSalesData();

        LOGGER.info("send message to kafka");
        kafkaProducer.sendMessage(salesReportData);
    }
}
