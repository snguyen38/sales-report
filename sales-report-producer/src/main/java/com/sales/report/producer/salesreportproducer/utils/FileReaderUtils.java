package com.sales.report.producer.salesreportproducer.utils;

import com.sales.report.models.SalesReportData;
import com.sales.report.models.SalesReportItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service
public class FileReaderUtils {
    private final FileCompressUtils fileCompressUtils;

    @Autowired
    public FileReaderUtils(FileCompressUtils fileCompressUtils) {
        this.fileCompressUtils = fileCompressUtils;
    }
    private static final Logger LOGGER = LoggerFactory.getLogger(FileReaderUtils.class);
    private static final String DUMB_DATA_DIRECTORY = "/dumb_data/";
    private static final String ARCHIVE_DATA_DIRECTORY = "archives";

    private static int readPosition = 0;

    // hardcode this cause this logic may not happen in real life
    private static final String[] FILE_LISTS = {
            "Sales_20221001_20221031.psv",
            "Sales_20221101_20221130.psv",
            "Sales_20221201_20221231.psv"
    };

    private static final Map<String, Map<String, Integer>> salesUnitsByProductAndStore = new HashMap<>();
    private static final Map<String, Map<String, BigDecimal>> salesRevenueByProductAndStore = new HashMap<>();

    public SalesReportData readFilesAndCalculateSalesData() {
        SalesReportData salesReportData = new SalesReportData();
        if (readPosition > 2) {
            readPosition = 0;
        }

        String filePath = DUMB_DATA_DIRECTORY + FILE_LISTS[readPosition];
        readPosition++;

        InputStream inputStream = FileReaderUtils.class.getResourceAsStream(filePath);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line = br.readLine(); // Skip header line
            while ((line = br.readLine()) != null) {
                String[] fields = line.split("\\|");
                String storeName = fields[1];
                String productName = fields[2];
                int salesUnits = Integer.parseInt(fields[3]);
                BigDecimal salesRevenue = new BigDecimal(fields[4]);

                // Calculate total sales units by product and store
                Map<String, Integer> salesUnitsByStore = salesUnitsByProductAndStore.computeIfAbsent(productName, k -> new HashMap<>());
                int totalSalesUnits = salesUnitsByStore.getOrDefault(storeName, 0) + salesUnits;
                salesUnitsByStore.put(storeName, totalSalesUnits);

                // Calculate total sales revenue by product and store
                Map<String, BigDecimal> salesRevenueByStore = salesRevenueByProductAndStore.computeIfAbsent(productName, k -> new HashMap<>());
                BigDecimal totalSalesRevenue = salesRevenueByStore.getOrDefault(storeName, BigDecimal.ZERO).add(salesRevenue);
                salesRevenueByStore.put(storeName, totalSalesRevenue);
            }
        } catch (IOException e) {
            LOGGER.error("Runtime error when read file", e);
            throw new RuntimeException(e);
        }

        for (String product : salesUnitsByProductAndStore.keySet()) {
            Map<String, Integer> salesUnitsByStore = salesUnitsByProductAndStore.get(product);
            Map<String, BigDecimal> salesRevenueByStore = salesRevenueByProductAndStore.get(product);
            for (String store : salesUnitsByStore.keySet()) {
                int salesUnits = salesUnitsByStore.get(store);
                BigDecimal salesRevenue = salesRevenueByStore.get(store);

                SalesReportItem reportItem = new SalesReportItem(product, store, salesUnits, salesRevenue.doubleValue());
                salesReportData.getReportMessages().add(reportItem);
            }
        }

        try {
            fileCompressUtils.compressFile(filePath, ARCHIVE_DATA_DIRECTORY);
        } catch (IOException e) {
            LOGGER.error("IOException when compress file", e);
        }


        return salesReportData;
    }
}
