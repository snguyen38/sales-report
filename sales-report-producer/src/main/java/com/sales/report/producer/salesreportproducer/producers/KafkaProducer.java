package com.sales.report.producer.salesreportproducer.producers;


import com.sales.report.models.SalesReportData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;


@Service
public class KafkaProducer {

    private static final Logger LOGGER = LogManager.getLogger(KafkaProducer.class);

    private final KafkaTemplate<String, SalesReportData> kafkaTemplate;

    @Autowired
    public KafkaProducer(KafkaTemplate<String, SalesReportData> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Value(value = "${kafka.topic}")
    private String topicName;

    /**
     * Send Kafka message to config topic ${kafka.topic}
     */
    public void sendMessage(SalesReportData message) {
        Message<SalesReportData> reportDataMessage = MessageBuilder.withPayload(message).setHeader(KafkaHeaders.TOPIC, topicName).build();

        LOGGER.info("Send message to kafka");
        CompletableFuture<SendResult<String,SalesReportData>> future = kafkaTemplate.send(reportDataMessage);
        future.whenComplete((result, exception) -> {
            if (exception == null) {
                LOGGER.info("Sent message success!");
            } else {
                LOGGER.info("Unable to send message cause: {} ", exception.getMessage());
            }
        });
    }
}
