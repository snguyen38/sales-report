# Sales Report Producer

This project reads a sales report file and publishes it to Kafka. After publishing, the file is zipped and moved to the archives' folder.

## Installation
1. Clone the repository
2. Install the necessary dependencies by running gradle command

    `.\gradlew clean build`
3. Navigate to the project's docker root directory

   `cd docker`
4. Run the service by running docker compose command in the root docker folder.
   Please note that Kafka has to be running already.

   `docker-compose up`

## Usage
1. Place the sales report file in the `resource/dumb_data` folder.
2. The schedule job run by every minute will read, calculate and publish the data to Kafka and then compress move the file to the archives folder.


## Configuration
The configuration file `application.properties` contains the following options:
* spring.kafka.server.url: The address of the Kafka broker to publish to.
* kafka.topic: The Kafka's topic
* server.port: The port of service will be running