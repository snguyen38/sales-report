package com.sales.report.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SalesReportData implements Serializable {
    private List<SalesReportItem> reportMessages = new ArrayList<>();

    public SalesReportData() {
    }

    public SalesReportData(List<SalesReportItem> reportMessages) {
        this.reportMessages = reportMessages;
    }

    public List<SalesReportItem> getReportMessages() {
        return reportMessages;
    }

    public void setReportMessages(List<SalesReportItem> reportMessages) {
        this.reportMessages = reportMessages;
    }
}
