package com.sales.report.models;

import java.io.Serializable;

public class SalesReportItem implements Serializable {
    private String product;
    private String store;
    private long totalSalesUnit;
    private double totalSalesRevenue;

    public SalesReportItem() {
    }

    public SalesReportItem(String product, String store, long totalSalesUnit, double totalSalesRevenue) {
        this.product = product;
        this.store = store;
        this.totalSalesUnit = totalSalesUnit;
        this.totalSalesRevenue = totalSalesRevenue;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public long getTotalSalesUnit() {
        return totalSalesUnit;
    }

    public void setTotalSalesUnit(long totalSalesUnit) {
        this.totalSalesUnit = totalSalesUnit;
    }

    public double getTotalSalesRevenue() {
        return totalSalesRevenue;
    }

    public void setTotalSalesRevenue(double totalSalesRevenue) {
        this.totalSalesRevenue = totalSalesRevenue;
    }
}

