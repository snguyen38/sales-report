# Demand Planning Development Task
Welcome to VNet Solutions' Demand Planning Development Task. This project aims to help Sales Manager Tom aggregate the company's sales data and display it in real-time.

## Requirements
Sales Manager Tom receives sales files on a daily basis and wants a web application that can help him aggregate data and display it in real-time. The requirements for the project are as follows:

* A Producer Java application that reads sales files and aggregates the sales data by product and store. The application should calculate the total sales units and total sales revenue and publish the result to an event platform (e.g. Kafka). The raw sales files should be archived in a separate folder.
* A Consumer Java application that listens to the event platform and pushes new messages to the website in real-time.
* A website frontend that displays the aggregated sales data in real-time without the need for page refreshes.

## Technologies Used
The project utilizes the following technologies:

### Backend
* Java 17
* Spring Boot 3
* Event platform - Apache Kafka
* STOMP websocket

### Frontend
* Node v18.15.0
* NPM 9.6.3
* Angular
* Angular CLI
* TypeScript


## Getting Started
To get started with the project, follow these steps:

1. Clone the repository to your local machine.
2. Navigate to the project's root directory in your terminal.
3. Install the necessary dependencies by running gradle build for the backend.

    `.\gradlew clean build`
4. Navigate to the project's sales-report-app directory in your terminal.

    `cd sales-report-app`
5. Install the necessary dependencies by running gradle build for the frontend.

    `npm install`
6. Build frontend app(optional)

    `ng build`
7. Navigate back to the project's root directory
8. Navigate to the project's docker root directory

   `cd docker`
9. Run the whole application by running docker compose command in the root docker folder.

    `docker-compose up`

   5 services will be running:
   * Zookeeper: To manage Kafka
   * Kafka: To be the main message system
   * Sales report producer: To read sales report files and publish to a Kafka topic
   * Sales report consumer: To subscribe on a Kafka topic receive message data then send to frontend via websocket
   * Sale report app: UI to display sales data in real-time
10. Navigate to http://localhost:4200 in your web browser.

### Note
The 5 services can either run together or individually. To find more details, please navigate to each specific service.


### Reference Documentation
* [Official Gradle documentation](https://docs.gradle.org)
* [Spring for Apache Kafka](https://docs.spring.io/spring-boot/docs/3.0.5/reference/htmlsingle/#messaging.kafka)

