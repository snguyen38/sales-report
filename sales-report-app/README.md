# SalesReportApp

This project provide UI to user can view real-time sales data 


## Installation
1. Clone the repository
2. Install the necessary dependencies by running npm command

   `npm install`
3. Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

4. Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
5. Navigate to the project's docker root directory

   `cd docker`
6. Run the service by running docker compose command in the root docker folder.
   Please note that Kafka has to be running already.

   `docker-compose up`

## UI
![img.png](img.png)
