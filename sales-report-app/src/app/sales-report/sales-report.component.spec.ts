import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { SalesReportComponent } from './sales-report.component';
import { WebsocketService } from '../services/websocket.service';

describe('SalesReportComponent', () => {
  let component: SalesReportComponent;
  let fixture: ComponentFixture<SalesReportComponent>;
  let mockWebSocketService: jasmine.SpyObj<WebsocketService>;

  beforeEach(async () => {
    mockWebSocketService = jasmine.createSpyObj('WebsocketService', ['connect', 'disconnect']);

    await TestBed.configureTestingModule({
      declarations: [ SalesReportComponent ],
      providers: [{ provide: WebsocketService, useValue: mockWebSocketService }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesReportComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render table', () => {
    const fixture = TestBed.createComponent(SalesReportComponent);
    const compiled = fixture.nativeElement as HTMLElement;
    const table = compiled.querySelector('table') as HTMLTableElement;
    expect(table).toBeTruthy();

    const headers = table.querySelectorAll('thead th');
    expect(headers.length).toBe(4);
    expect(headers[0].textContent).toBe('Product');
    expect(headers[1].textContent).toBe('Store');
    expect(headers[2].textContent).toBe('Total Sales Units');
    expect(headers[3].textContent).toBe('Total Sales Revenue');

    const rows = table.querySelectorAll('tbody tr');
    expect(rows.length).toBe(fixture.componentInstance.salesData.length);
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];
      const sale = fixture.componentInstance.salesData[i];
      const cells = row.querySelectorAll('td');
      expect(cells.length).toBe(4);
      expect(cells[0].textContent).toBe(sale.product);
      expect(cells[1].textContent).toBe(sale.store);
      expect(cells[2].textContent).toBe(`${sale.totalSalesUnit}`);
      expect(cells[3].textContent).toBe(`${sale.totalSalesRevenue} USD`);
    }
  });

  it('should subscribe to websocket service and receive sales data', () => {
    const mockSalesData = [
      { product: 'Product A', store: 'Store 1', totalSalesUnit: 10, totalSalesRevenue: 100 },
      { product: 'Product B', store: 'Store 2', totalSalesUnit: 5, totalSalesRevenue: 50 }
    ];
    const mockMessage = { reportMessages: mockSalesData };
    mockWebSocketService.connect.and.returnValue(of(JSON.stringify(mockMessage)));

    fixture.detectChanges();
    expect(mockWebSocketService.connect).toHaveBeenCalled();
    expect(component.salesData).toEqual(mockSalesData);
  });

  it('should unsubscribe from websocket service on component destroy', () => {
    fixture.destroy();
    expect(mockWebSocketService.disconnect).toHaveBeenCalled();
  });
});
