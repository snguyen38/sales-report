import {Component, OnInit, OnDestroy} from '@angular/core';
import {WebsocketService} from "../services/websocket.service";
import {SalesReportItem} from "../models/sales-report-item";

@Component({
  selector: 'app-sales-report',
  templateUrl: './sales-report.component.html',
  styleUrls: ['./sales-report.component.css']
})
export class SalesReportComponent implements OnInit, OnDestroy {

  constructor(private websocketService: WebsocketService) { }

  salesData: SalesReportItem[] = [];

  ngOnInit(): void {
    this.websocketService.connect().subscribe((message) => {
        if (!!message) {
          this.salesData = JSON.parse(message).reportMessages;
        }
      });
  }

  ngOnDestroy(): void {
    if (!!this.websocketService) {
      this.websocketService.disconnect();
    }
  }

}
