import {Injectable} from '@angular/core';
// @ts-ignore
import * as SockJS from 'sockjs-client/dist/sockjs.js';
import {BehaviorSubject, Observable} from 'rxjs';
import {CompatClient, IMessage, Stomp} from '@stomp/stompjs';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  private socketUrl = 'http://localhost:8080/ws';
  private stompClient: CompatClient | undefined;
  private messagesSubject = new BehaviorSubject<string>("");

  constructor() {}

  connect(): Observable<string> {
    const socket = new SockJS(this.socketUrl);
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, () => {
      // @ts-ignore
      this.stompClient.subscribe('/topic/websocket', (message: IMessage) => {
        this.messagesSubject.next(message.body);
      });
    });
    return this.messagesSubject.asObservable();
  }

  disconnect(): void {
    this.stompClient && this.stompClient.disconnect(() => {
      console.log('disconnected from websocket');
    });
  }
}
