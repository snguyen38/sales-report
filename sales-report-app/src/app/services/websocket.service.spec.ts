import { TestBed } from '@angular/core/testing';
import { WebsocketService } from './websocket.service';
import {Observable} from "rxjs";

describe('WebsocketService', () => {
  let service: WebsocketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebsocketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should connect to websocket server', () => {
    spyOn(service, 'connect').and.callThrough();
    const messages$: Observable<string> = service.connect();
    expect(service.connect).toHaveBeenCalled();
    messages$.subscribe(() => {
      service.disconnect();
    });
  });

  it('should disconnect from websocket server', () => {
    spyOn(service, 'disconnect').and.callThrough();
    service.disconnect();
    expect(service.disconnect).toHaveBeenCalled();
  });
});
