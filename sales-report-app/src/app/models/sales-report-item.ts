export interface SalesReportItem {
  product: string;
  store: string;
  totalSalesUnit: number;
  totalSalesRevenue: number;
}
