import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {SalesReportComponent} from "./sales-report/sales-report.component";

describe('AppComponent & SalesReportComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent, SalesReportComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

});
